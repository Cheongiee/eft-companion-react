import SQLite from "react-native-sqlite-storage";
import { DatabaseInitialization } from "./DatabaseInitialization";

export interface Database {
    open(): Promise<SQLite.SQLiteDatabase>;
    close(): Promise<void>;
    // Define CRUD functions here
}

class DatabaseImpl implements Database {
    private databaseName = "AppDatabase.db";
    private database: SQLite.SQLiteDatabase | undefined;

    // Open the connection to the database
    public open(): Promise<SQLite.SQLiteDatabase> {
        ...
    }

    // Close the connection to the database
    public close(): Promise<void> {
        ...
    }

    // CRUD operation code goes here
}

// Export a single instance of DatabaseImpl
export const database: Database = new DatabaseImpl();