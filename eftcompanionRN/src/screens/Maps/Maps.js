import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import ImageZoom from 'react-native-image-pan-zoom';
import FastImage from 'react-native-fast-image';
import mapsIndex from '../../assets/images/index/mapsIndex';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class MapsScreen extends Component {
  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      mapsArray: ['Factory', 'Customs', 'Woods', 'Shoreline', 'Interchange']
    };
  }

  handleIndexChange = index => {
    this.setState({
      ...this.state,
      selectedIndex: index
    });
  };

  render() {
    const mapRender = mapsIndex[this.state.mapsArray[this.state.selectedIndex]];

    return (
      <View style={styles.container}>
        <Text style={{ color: '#FDFFFC', fontWeight: 'bold' }}>
          {this.state.mapsArray[this.state.selectedIndex]}
        </Text>
        <SegmentedControlTab
          values={this.state.mapsArray}
          selectedIndex={this.state.selectedIndex}
          onTabPress={this.handleIndexChange}
          tabStyle={{ backgroundColor: 'transparent' }}
        />
        <View style={styles.container}>
          <ImageZoom
            style={styles.mapsContainer}
            cropWidth={windowWidth}
            cropHeight={windowHeight}
            imageWidth={windowWidth}
            imageHeight={windowHeight}
            maxScale={20}
          >
            <FastImage
              style={styles.imageContainer}
              source={mapRender}
              resizeMode={FastImage.resizeMode.contain}
            />
          </ImageZoom>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  },
  mapsContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  imageContainer: {
    width: '100%',
    height: '100%'
    // resizeMode: 'contain'
  }
});

export default MapsScreen;
