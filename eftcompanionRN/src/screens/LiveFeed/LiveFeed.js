import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { WebView } from 'react-native-webview';
// import NavigationService from '../../services/NavigationService/NavigationService';

class LiveFeedScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <WebView
          style={styles.container}
          startInLoadingState={true}
          source={{
            uri: 'https://www.twitch.tv/directory/game/Escape%20From%20Tarkov'
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#141414'
  }
});

export default LiveFeedScreen;
