import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import FastImage from 'react-native-fast-image';
import imagesUIIndex from '../../assets/images/index/uiIndex';

const db = openDatabase({ name: 'eftdataraw.db', createFromLocation: 1 });

const weaponClass = 'Weapon';
const modClass = 'Mod';

class WeaponModsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: [],
      FlatListMods: []
    };

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM EFTModel WHERE [ItemClass] = ?',
        [weaponClass],
        (tx, results) => {
          let temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          // var temp1 = temp.filter(function(el) {
          //   return el.SubType == 'Shotgun';
          // });
          let temp1 = temp
            .map(item => item.Type)
            .filter((value, index, self) => self.indexOf(value) === index);
          console.log(temp1);
          this.setState({
            FlatListItems: temp1
          });
        }
      );
    });

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM EFTModel WHERE [ItemClass] = ?',
        [modClass],
        (tx, results) => {
          let tempMod = [];
          for (let i = 0; i < results.rows.length; ++i) {
            tempMod.push(results.rows.item(i));
          }
          // var temp1 = temp.filter(function(el) {
          //   return el.SubType == 'Shotgun';
          // });
          let tempMod1 = tempMod
            .map(item => item.Type)
            .filter((value, index, self) => self.indexOf(value) === index);
          console.log(tempMod1);
          this.setState({
            FlatListMods: tempMod1
          });
        }
      );
    });
  }
  ListViewItemSeparator = () => {
    return (
      <View
        style={{ height: 0.2, width: '100%', backgroundColor: '#808080' }}
      />
    );
  };

  selectTypeHandler = item => {
    this.props.navigation.navigate('WeaponTypes', {
      itemTypeSelected: item
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: '#282828',
            padding: 5,
            paddingLeft: 20,
            width: '100%'
          }}
        >
          <Text style={{ color: '#FDFFFC', fontWeight: 'bold' }}>WEAPONS</Text>
        </View>
        <FlatList
          style={{ backgroundColor: 'transparent', width: '100%' }}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          data={this.state.FlatListItems}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={this.selectTypeHandler.bind(this, item)}>
              <View
                style={{
                  backgroundColor: 'transparent',
                  padding: 20,
                  width: '100%',
                  flexDirection: 'row'
                }}
              >
                <FastImage
                  source={imagesUIIndex[item]}
                  style={styles.imageContainer}
                />
                <Text style={{ color: '#FDFFFC' }}>{item}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
        <View
          style={{
            backgroundColor: '#282828',
            padding: 5,
            paddingLeft: 20,
            width: '100%'
          }}
        >
          <Text style={{ color: '#FDFFFC', fontWeight: 'bold' }}>MODS</Text>
        </View>
        <FlatList
          style={{ backgroundColor: 'transparent', width: '100%' }}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          data={this.state.FlatListMods}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={this.selectTypeHandler.bind(this, item)}>
              <View
                style={{
                  backgroundColor: 'transparent',
                  padding: 20,
                  width: '100%'
                }}
              >
                <Text style={{ color: '#FDFFFC' }}>{item}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#141414'
  },
  imageContainer: {
    width: 40,
    height: 30,
    marginRight: 10
  }
});

export default WeaponModsScreen;
