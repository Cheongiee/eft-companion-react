import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import FastImage from 'react-native-fast-image';
import imagesWeaponModsIndex from '../../assets/images/index/weaponmodsIndex';

const db = openDatabase({ name: 'eftdataraw.db', createFromLocation: 1 });
const DeviceWidth = Dimensions.get('window').width;

class WeaponDetailsScreen extends Component {
  static navigationOptions = {
    title: 'DETAILS'
  };
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: [],
      userData: ''
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const itemDetailsSelected = navigation.getParam(
      'itemDetailsSelected',
      'NO-ID'
    );

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM EFTModel WHERE [ShortName] = ?',
        [itemDetailsSelected],
        (tx, results) => {
          let temp = results.rows.length;
          // console.log('len', len);
          if (temp > 0) {
            this.setState({
              userData: results.rows.item(0)
            });
            console.log(userData);
          } else {
            alert('No user found');
            this.setState({
              userData: ''
            });
          }
        }
      );
    });
  }

  ListViewItemSeparator = () => {
    return (
      <View
        style={{ height: 0.2, width: '100%', backgroundColor: '#808080' }}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textContainer}>
          {this.state.userData.ShortName}
        </Text>
        <FastImage
          source={imagesWeaponModsIndex[this.state.userData.ID]}
          style={styles.imageContainer}
          resizeMode='contain'
        />
        <View
          style={{
            // flex: 1,
            // alignSelf: 'stretch',
            flexDirection: 'row',
            width: '100%',
            backgroundColor: 'blue'
          }}
        >
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Recoil: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Recoil Back: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
        </View>
        <View
          style={{
            // flex: 1,
            // alignSelf: 'stretch',
            flexDirection: 'row',
            width: '100%'
          }}
        >
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Ergonomics: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              Effective Distance:{' '}
            </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
        </View>
        <View
          style={{
            // flex: 1,
            // alignSelf: 'stretch',
            flexDirection: 'row',
            width: '100%'
          }}
        >
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Firing Rate: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Accuracy: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
        </View>
        <View
          style={{
            // flex: 1,
            // alignSelf: 'stretch',
            flexDirection: 'row',
            width: '100%'
          }}
        >
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Muzzle Velocity: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>Type: </Text>
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <Text style={styles.textDetailsContainer}>
              {this.state.userData.RPM ? this.state.userData.RPM : 0}
            </Text>
          </View>
        </View>
        <View style={styles.container} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: 'grey'
  },
  textContainer: {
    color: 'white',
    textAlign: 'center',
    // fontSize: 10,
    padding: 5
  },
  textDetailsContainer: {
    color: 'white',
    // textAlign: 'center',
    // fontSize: 10,
    padding: 5
  },
  imageContainer: {
    width: '90%',
    height: 80,
    marginHorizontal: 2
  },
  tableContainer: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff'
  },
  textGrid: {
    color: 'white'
  }
});

export default WeaponDetailsScreen;
