import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import FastImage from 'react-native-fast-image';
import imagesWeaponModsIndex from '../../assets/images/index/weaponmodsIndex';

const db = openDatabase({ name: 'eftdataraw.db', createFromLocation: 1 });

class WeaponSubTypesScreen extends Component {
  // static navigationOptions = {
  //   title: 'WEAPON TYPES'
  // };
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: []
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const itemSubType = navigation.getParam('itemSubTypeSelected', 'NO-ID');
    console.log(itemSubType);

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM EFTModel WHERE [SubType] = ?',
        [itemSubType],
        (tx, results) => {
          let temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          this.setState({
            FlatListItems: temp
          });
        }
      );
    });
  }

  ListViewItemSeparator = () => {
    return (
      <View
        style={{ height: 0.2, width: '100%', backgroundColor: '#808080' }}
      />
    );
  };

  selectItemDetailsHandler = item => {
    this.props.navigation.navigate('WeaponDetails', {
      itemDetailsSelected: item
    });
  };

  defineBgColor(key) {
    switch (key) {
      case key > 0:
        return styles.textRedContainer;
      case key < 0:
        return styles.textGreenContainer;
      default:
        return styles.textGreenContainer;
    }
  }

  render() {
    const { navigation } = this.props;
    const itemSubType = navigation.getParam('itemSubTypeSelected', 'NO-ID');

    return (
      <View style={styles.container}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            backgroundColor: '#282828',
            paddingLeft: 20,
            width: '100%'
          }}
        >
          <Text style={{ color: '#FDFFFC', fontWeight: 'bold', padding: 5 }}>
            {itemSubType}
          </Text>
          <View style={styles.endContainer}>
            <Text style={styles.legendContainer}>R</Text>

            <Text style={styles.legendContainer}>E</Text>
            <Text style={styles.legendContainer}>F</Text>
            <Text style={styles.legendContainer}>M</Text>
          </View>
        </View>
        <View style={styles.container}>
          <FlatList
            style={{ width: '100%' }}
            ItemSeparatorComponent={this.ListViewItemSeparator}
            data={this.state.FlatListItems}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={this.selectItemDetailsHandler.bind(
                  this,
                  item.ShortName
                )}
              >
                <View style={styles.listItemsContainer}>
                  <FastImage
                    source={imagesWeaponModsIndex[item.ID]}
                    style={styles.imageContainer}
                    resizeMode='contain'
                  />
                  <View style={styles.listTextContaner}>
                    <Text style={styles.textNameContainer}>
                      {item.ShortName}
                    </Text>
                    <Text style={styles.textDetailsContainer}>
                      {item.Cartridge}
                    </Text>
                  </View>
                  <View style={styles.endContainer}>
                    <View style={styles.endListContainer}>
                      <Text
                        style={
                          item.RecoilUp > 0
                            ? styles.textRedContainer
                            : item.RecoilUp == 0
                            ? styles.textGreenContainer
                            : styles.textContainer
                        }
                      >
                        {item.RecoilUp ? item.RecoilUp : 0}
                      </Text>
                    </View>
                    <View style={styles.endListContainer}>
                      <Text style={this.defineBgColor(item.Ergonomics)}>
                        {item.Ergonomics ? item.Ergonomics : 0}
                      </Text>
                    </View>
                    <View style={styles.endListContainer}>
                      <Text style={styles.textContainer}>
                        {item.RPM ? item.RPM : 0}
                      </Text>
                    </View>
                    <View style={styles.endListContainer}>
                      <Text style={styles.textContainer}>
                        {item.MuzzleVelocity ? item.MuzzleVelocity : 0}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#141414'
  },
  endContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: '100%'
  },
  listItemsContainer: {
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  listTextContaner: {
    flex: 2,
    flexDirection: 'column'
  },
  textNameContainer: {
    color: '#FDFFFC',
    paddingLeft: 3,
    fontSize: 10,
    paddingTop: 10,
    paddingBottom: 3
  },
  textDetailsContainer: {
    color: '#FDFFFC',
    paddingLeft: 3,
    fontSize: 10,
    color: 'grey',
    paddingTop: 3,
    paddingBottom: 10
  },
  legendContainer: {
    textAlign: 'center',
    padding: 5,
    marginHorizontal: 1,
    height: '100%',
    width: 30,
    backgroundColor: 'black',
    color: 'grey',
    fontWeight: 'bold'
  },
  textContainer: {
    textAlign: 'center',
    alignSelf: 'center',
    width: 30,
    backgroundColor: 'black',
    color: 'grey',
    fontSize: 9
  },
  textRedContainer: {
    textAlign: 'center',
    alignSelf: 'center',
    width: 30,
    backgroundColor: 'black',
    color: 'red',
    fontSize: 9
  },
  textGreenContainer: {
    textAlign: 'center',
    alignSelf: 'center',
    width: 30,
    backgroundColor: 'black',
    color: 'lime',
    fontSize: 9
  },
  endListContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: '100%',
    marginHorizontal: 1,
    // marginVertical: 0.5,
    width: 30,
    backgroundColor: 'black'
  },
  imageContainer: {
    width: 60,
    marginHorizontal: 2
  }
});

export default WeaponSubTypesScreen;
