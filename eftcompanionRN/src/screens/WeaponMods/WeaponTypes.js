import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import FastImage from 'react-native-fast-image';
import imagesUIIndex from '../../assets/images/index/uiIndex';

const db = openDatabase({ name: 'eftdataraw.db', createFromLocation: 1 });

class WeaponTypesScreen extends Component {
  // static navigationOptions = {
  //   title: itemType
  // };
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: []
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const itemType = navigation.getParam('itemTypeSelected', 'NO-ID');
    console.log(itemType);

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM EFTModel WHERE [Type] = ?',
        [itemType],
        (tx, results) => {
          let temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          let temp1 = temp
            .map(item => item.SubType)
            .filter((value, index, self) => self.indexOf(value) === index);
          console.log(temp1);
          console.log('DONE!!!!');
          this.setState({
            FlatListItems: temp1
          });
        }
      );
    });
  }

  ListViewItemSeparator = () => {
    return (
      <View
        style={{ height: 0.2, width: '100%', backgroundColor: '#808080' }}
      />
    );
  };

  selectSubTypeHandler = item => {
    this.props.navigation.navigate('WeaponSubTypes', {
      itemSubTypeSelected: item
    });
  };

  render() {
    const { navigation } = this.props;
    const itemType = navigation.getParam('itemTypeSelected', 'NO-ID');
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: '#282828',
            padding: 5,
            paddingLeft: 20,
            width: '100%'
          }}
        >
          <Text style={{ color: '#FDFFFC', fontWeight: 'bold' }}>
            {itemType}
          </Text>
        </View>
        <View style={styles.container}>
          <FlatList
            style={{ width: '100%' }}
            ItemSeparatorComponent={this.ListViewItemSeparator}
            data={this.state.FlatListItems}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={this.selectSubTypeHandler.bind(this, item)}
              >
                <View
                  style={{
                    padding: 20,
                    width: '100%',
                    flexDirection: 'row'
                  }}
                >
                  <FastImage
                    source={imagesUIIndex[item]}
                    style={styles.imageContainer}
                  />
                  <Text style={{ color: '#FDFFFC' }}>{item}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#141414'
  },
  imageContainer: {
    width: 40,
    height: 30,
    marginRight: 10
  }
});

export default WeaponTypesScreen;
