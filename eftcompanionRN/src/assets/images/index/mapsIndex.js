export default {
  Customs: require('../maps/customs.jpg'),
  Factory: require('../maps/factory.jpg'),
  Interchange: require('../maps/interchange.jpg'),
  Shoreline: require('../maps/shoreline.jpg'),
  Woods: require('../maps/woods.jpg')
};
