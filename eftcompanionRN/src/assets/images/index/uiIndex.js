export default {
  'Assault Carbine': require('../UI/carbine.png'),
  Throwable: require('../UI/frag.png'),
  'Melee Weapon': require('../UI/knife.png'),
  'Designated Marksman Rifle': require('../UI/marksman.png'),
  Secondary: require('../UI/pistol.png'),
  Primary: require('../UI/rifle.png'),
  Shotgun: require('../UI/shotgun.png'),
  'Submachine Gun': require('../UI/smg.png'),
  'Smoke Grenade': require('../UI/smoke.png'),
  'Sniper Rifle': require('../UI/sniper.png'),
  'Stun Grenade': require('../UI/stun.png'),
  'Assault Rifle': require('../UI/rifle.png'),
  'Fragmentation Grenade': require('../UI/frag.png'),
  Pistol: require('../UI/pistol.png')
};
