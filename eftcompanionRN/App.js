import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import {
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
  createStackNavigator,
  createTabNavigator,
  DrawerItems
} from 'react-navigation';
import NavigationService from './src/services/NavigationService/NavigationService';
// import { Provider } from 'react-redux';

import LiveFeedScreen from './src/screens/LiveFeed/LiveFeed';
import MapsScreen from './src/screens/Maps/Maps';
import WeaponModsScreen from './src/screens/WeaponMods/WeaponMods';
import WeaponTypesScreen from './src/screens/WeaponMods/WeaponTypes';
import WeaponSubTypesScreen from './src/screens/WeaponMods/WeaponSubTypes';
import WeaponDetailsSceen from './src/screens/WeaponMods/WeaponDetails';

// import configureStore from './src/store/configureStore';

export default class App extends Component {
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

//WEAPON MODS STACK
const WeaponModsStack = createStackNavigator({
  WeaponMods: {
    screen: WeaponModsScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'WEAPON MODS',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  },
  WeaponTypes: {
    screen: WeaponTypesScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'WEAPON MODS',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  },
  WeaponSubTypes: {
    screen: WeaponSubTypesScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'WEAPON MODS',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  },
  WeaponDetails: {
    screen: WeaponDetailsSceen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'WEAPON MODS',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  }
});

WeaponModsStack.navigationOptions = {
  tabBarLabel: 'Weapon Mods',
  tabBarIcon: ({ tintColor }) => (
    <Icon
      name='ios-pizza'
      containerStyle={{ marginTop: 6 }}
      size={30}
      color={tintColor}
    />
  )
};

//MAP STACK
const MapsStack = createStackNavigator({
  Feed: {
    screen: MapsScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'MAPS',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  }
});
MapsStack.navigationOptions = {
  tabBarLabel: 'Maps',
  tabBarIcon: ({ tintColor }) => (
    <Icon
      name='ios-planet'
      containerStyle={{ marginTop: 6 }}
      size={35}
      color={tintColor}
    />
  )
};

//LIVE STACK
const LiveFeedStack = createStackNavigator({
  Feed: {
    screen: LiveFeedScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'LIVE FEED',
        headerTintColor: '#FD9827',
        headerStyle: {
          backgroundColor: '#252525'
        }
      };
    }
  }
});
LiveFeedStack.navigationOptions = {
  tabBarLabel: 'Live Feed',
  tabBarIcon: ({ tintColor }) => (
    <Icon
      name='ios-flame'
      containerStyle={{ marginTop: 6 }}
      size={30}
      color={tintColor}
    />
  )
};

const DashboardTabNavigator = createBottomTabNavigator(
  {
    WeaponModsStack,
    MapsStack,
    LiveFeedStack
  },
  {
    tabBarOptions: {
      tabStyle: {
        paddingVertical: 2
      },
      activeTintColor: '#FD9827',
      inactiveTintColor: 'gray',
      style: {
        backgroundColor: '#252525'
      }
    }
  }
);

const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: DashboardTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        header: null
      };
    }
  }
);

//DRAWER
// const AppTabNavigator = createDrawerNavigator({
//   Dashboard: { screen: DashboardStackNavigator }
// });

const AppContainer = createAppContainer(DashboardStackNavigator);
